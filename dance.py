from time import sleep
from SCSCtrl import TTLServo
from jetbot import Robot
robot = Robot()

# Reset motors
for i in range(1,6):TTLServo.servoAngleCtrl(i, 0, 1, 200)
sleep(3)

# Move to side
TTLServo.servoAngleCtrl(1, 90, 1, 250)
sleep(2)

# Start moving back and forth while arm and camera meet and seperate
for i in range(2):
    TTLServo.xyInput(100, -20)
    TTLServo.servoAngleCtrl(5, -30, 1, 250)
    TTLServo.servoAngleCtrl(1, -90, 1, 250)
    sleep(3)
    TTLServo.servoAngleCtrl(1, 90, 1, 250)
    TTLServo.xyInput(1, 300)
    TTLServo.servoAngleCtrl(5, 30, 1, 250)
    sleep(2)
    
# Reset again
TTLServo.xyInput(150, 100)
TTLServo.servoAngleCtrl(5, 0, 1, 250)
TTLServo.servoAngleCtrl(1, 0, 1, 250)

# Move forward and backwards
robot.forward(0.7)
sleep(0.5)
robot.stop()
sleep(0.5)
robot.backward(0.7)
sleep(0.5)
robot.stop()

# Rotate left and right
robot.right(0.3)
sleep(1)
robot.stop()
sleep(0.5)
robot.left(0.3)
sleep(1)
robot.stop()
sleep(1)

# Clap with claw
for i in range(3):
    TTLServo.servoAngleCtrl(4, -60, 1, 500)
    sleep(1)
    TTLServo.servoAngleCtrl(4, 0, 1, 500)
    sleep(1)